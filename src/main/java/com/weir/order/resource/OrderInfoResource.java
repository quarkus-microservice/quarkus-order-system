package com.weir.order.resource;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.weir.order.entity.OrderInfo;
import com.weir.order.entity.OrderItem;
import com.weir.order.rest.goods.GoodsClientService;
import com.weir.order.rest.member.MemberClientService;
import com.weir.order.vo.OrderInfoVo;
import com.weir.quarkus.commons.utils.ChineseName;
import com.weir.quarkus.commons.utils.RandomPhoneNumGenerator;
import com.weir.quarkus.commons.utils.SFExpressNumberGenerator;
import com.weir.quarkus.commons.utils.Sequence;
import com.weir.quarkus.commons.utils.VuePageUtil;
import com.weir.quarkus.commons.utils.OrderNoCreate;
import com.weir.quarkus.commons.vo.goods.GoodsInfoVo;
import com.weir.quarkus.commons.vo.member.MemberInfoVo;
import com.weir.quarkus.commons.vo.page.VuePageListVo;
import com.weir.quarkus.commons.vo.result.ResultDataVo;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

@Tag(name = "订单管理")
@Path("/order-info")
@ApplicationScoped
public class OrderInfoResource {
	
	@Inject
    @RestClient
	GoodsClientService goodsClientService;
	@Inject
	@RestClient
	MemberClientService memberClientService;
	
	@Inject
	EntityManager em;
	
	private final Sequence sequence = new Sequence(null);
	
	@GET
	@Path("/addBatch")
	@Transactional
	public String addBatch() {
		long a = System.currentTimeMillis();
		List<OrderInfo> orderInfos = new ArrayList<>();
		List<OrderItem> orderItems = new ArrayList<>();
		for (int i = 0; i < 10000; i++) {
			OrderInfo orderInfo = new OrderInfo();
			orderInfo.id = sequence.nextId();
			orderInfo.clientType = "PC";
			orderInfo.consigneeAddressPath = "龙城镇钮王村";
			orderInfo.consigneeDetail = "龙城镇钮王村";
			orderInfo.consigneeMobile = RandomPhoneNumGenerator.generatePhoneNum();
			orderInfo.consigneeName = ChineseName.getName();
			orderInfo.deliverStatus = "UNDELIVERED";
			orderInfo.deliveryMethod = "LOGISTICS";
			orderInfo.discountPrice = 30d;
			orderInfo.freightPrice = 600d;
			orderInfo.goodsNum = 2;
			orderInfo.goodsPrice = 21.3;
			orderInfo.logisticsCode = "SF";
			orderInfo.logisticsName = "顺丰快递";
			orderInfo.logisticsNo = SFExpressNumberGenerator.generateSFExpressNumber();
			orderInfo.logisticsTime = new Date();
			orderInfo.memberId = sequence.nextId().toString();
			orderInfo.memberName = ChineseName.getName();
			orderInfo.needReceipt = false;
			orderInfo.orderCount = 4;
			orderInfo.orderMoney = 45.5;
			orderInfo.orderNo = OrderNoCreate.randomOrderCode();
			orderInfo.orderPromotionType = "NORMAL";
			orderInfo.orderStatus = "UNDELIVERED";
			orderInfo.orderType = "NORMAL";
			orderInfo.paymentMethod = "WECHAT";
			orderInfo.paymentTime = new Date();
			orderInfo.payStatus = "UNPAID";
			orderInfo.storeName = "路口商店";
			orderInfos.add(orderInfo);
//			orderInfo.persist();
			for (int j = 0; j < 3; j++) {
				OrderItem orderItem = new OrderItem();
				orderItem.id = sequence.nextId();
				orderItem.afterSaleStatus = "NOT_APPLIED";
				orderItem.categoryId = Integer.valueOf(i+1).toString();
				orderItem.createTime = new Date();
				orderItem.creator = i+1;
				orderItem.flowPrice = 44.5;
				orderItem.goodId = i+1;
				orderItem.goodMoney = 33.5;
				orderItem.goodsName = "冰城雪梨";
				orderItem.goodsPrice = 4.5;
				orderItem.image = "xxx/xxxxxxx/xxxxxxx.jpg";
				orderItem.memberId = i+1;
				orderItem.num = 2;
				orderItem.orderId = orderInfo.id;
				orderItem.orderItemSn = orderInfo.orderNo + j + 1;
				orderItems.add(orderItem);
//				orderItem.persist();
			}
		}
		long a2 = System.currentTimeMillis();
		OrderInfo.persist(orderInfos);
		OrderItem.persist(orderItems);
		long b = System.currentTimeMillis();
		return "耗时："+ (b - a) +"-------" + (a2 - a);
	}
	
	@Operation(summary = "微服务之间调用实现")
	@GET
	@Path("/add/{memberId}")
	public OrderInfoVo addOrder(@PathParam("memberId") Integer memberId) {

		// 这里是微服务内部调用
		
		List<GoodsInfoVo> goodList = goodsClientService.list(memberId);
		MemberInfoVo address = memberClientService.getAddress(memberId);
		
		OrderInfoVo orderInfo = new OrderInfoVo();
		orderInfo.goodList = goodList;
		orderInfo.address = address;
		
		return orderInfo;
	}

	@Operation(summary = "订单分页列表")
	@GET
	@Path("/list")
    public Response list(@QueryParam("page") Integer page, @QueryParam("pageSize") Integer pageSize) {
		PanacheQuery<PanacheEntityBase> panacheQuery = OrderInfo.findAll().page(page, pageSize);
		VuePageListVo<OrderInfo> list = VuePageUtil.toPage(panacheQuery.list(), 
				panacheQuery.count(), new VuePageListVo<OrderInfo>(), page, pageSize);
		return Response.ok().entity(new ResultDataVo<>(200, "成功" , list)).build();
	}
	
	
}

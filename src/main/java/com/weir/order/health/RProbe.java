package com.weir.order.health;

import jakarta.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Readiness;
/**
 * 就绪探针
 * @author weir
 *
 */
@Readiness
@ApplicationScoped
public class RProbe implements HealthCheck {

    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.up("I'm order ready!");
    }
    
}
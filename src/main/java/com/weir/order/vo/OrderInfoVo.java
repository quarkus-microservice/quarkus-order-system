package com.weir.order.vo;

import java.util.List;

import com.weir.quarkus.commons.vo.goods.GoodsInfoVo;
import com.weir.quarkus.commons.vo.member.MemberInfoVo;

public class OrderInfoVo {

	public List<GoodsInfoVo> goodList;
	public MemberInfoVo address;
}

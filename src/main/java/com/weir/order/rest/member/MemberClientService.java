package com.weir.order.rest.member;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.weir.quarkus.commons.vo.member.MemberInfoVo;

@Path("member/info")
@RegisterRestClient
public interface MemberClientService {

	@GET
	@Path("address/list/{memberId}")
	public MemberInfoVo getAddress(@PathParam("memberId") Integer memberId);
}

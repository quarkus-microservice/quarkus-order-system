package com.weir.order.rest.goods;

import java.util.List;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.weir.quarkus.commons.vo.goods.GoodsInfoVo;

@Path("goods/cart")
@RegisterRestClient
public interface GoodsClientService {

	@Operation(summary = "获取会员购物车商品")
	@GET
	@Path("/list/{memberId}")
	public List<GoodsInfoVo> list(@PathParam("memberId") Integer memberId);
}
